<?php

namespace App\Http\Controllers;
  
use Illuminate\Http\Request;
use Rap2hpoutre\FastExcel\FastExcel;
use App\User;
use Illuminate\Support\Facades\Storage;
use DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;


  
class MyController extends Controller
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function importExportView()
    {
       return view('import_excel');
    }
   
    /**
    * @return \Illuminate\Support\Collection
    */
    public function export() 
    {
        $users = User::all();

		// Export all users
		return (new FastExcel($users))->export('file.xlsx');
    }
   
    /**
    * @return \Illuminate\Support\Collection
    */
    public function import(Request $request) 
    {
		$rules = array(
        'file' => 'required|mimes:xls,xlsx',
        );
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails())
		{
			return back()->withErrors($validator);
		}
		else
		{
			if(Input::hasFile('file'))
			{
				$data = file_get_contents(Input::file('file')->getRealPath());
				DB::table('files')->insert(array(
					'data'=> $data,
					'name'=> 'test.xls'
					)
				);
				return 'ok';
			}
		}
		//return $request->file('file');
		//$file = $request->file('file')->storeAs('subscription', 'file.xlsx'  ,'custom');
		//die;
		//Storage::disk('local')->putFileAs('uploads', $request->file('file'),'file.xlsx');
       //return $collection = (new FastExcel)->import('file.xlsx');
	   $users = (new FastExcel)->import('uploads/subscription/file.xlsx', function ($line) {
		   //echo 122;die;
		  // print_r($line);die;
			if($line['name']!=""){
				$user = User::create([
					'name' => $line['name'],
					'email' => $line['email'],
					'password' => bcrypt('111111111')
				]);
				DB::table('user_role')->insert(['email' => $user->email, 'user_id' => $user->id]);
			}
			//return $user;
			/* print_r($user);
			die; */
		});

           
        return back();
    }
}
